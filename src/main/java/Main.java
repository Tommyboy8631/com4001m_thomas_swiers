import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.lang.*;
import static java.lang.Math.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends javax.swing.JFrame {

//ArrayList to store the basket and shop info..
    ArrayList<Shop> shop = new ArrayList<Shop>();
    ArrayList<Basket> basket = new ArrayList<Basket>();
    
    // the user option to the user when in the enter button method
        int userOption = 0;
        // code of the currant item that the user is adding the to the basket
        int itemCode;
        //The number of items that the user has added to their basket
        int itemNumber;
        
        double totalCost;
        
        FileWriter writer = new FileWriter("receipt.txt");
    
    public Main() throws IOException {
        initComponents();
        
       
        //Start of my Main Code
        
        
        labelBox.setText("Welcome to the shopping self checkout ...");
        textBox.setText("Please select one of the options below to begin \n"
                + "1 - add an item to the basket \n"
                + "2 - \n"
                + "3 - look at the list of items in the store \n"
                + "4 - checkout");
        
        
        // putting item inside the shop class to the used later
        shop.add(new Shop(0, "Pen", 1, 20));
        shop.add(new Shop(1, "Book", 1, 20));
        shop.add(new Shop(2, "Water", 1, 20));
        shop.add(new Shop(3, "Drink", 1, 20));
        shop.add(new Shop(4, "Snack", 2, 20));
        shop.add(new Shop(5, "Sweet", 1, 20));
        shop.add(new Shop(6, "Chocolate", 1, 20));
        shop.add(new Shop(7, "Biscuits", 1, 20));
        shop.add(new Shop(8, "Cake", 2, 20));
        
        //start of the purchase listing 
        //TODO change the box so it starts displaying at the top of the box ...
        purchaseListingBox.setText("Code\t Name\t Price\n");
       
        /*
        
          I am sorry but this project doesnt really work for some reason the
        items dont get added to the array or something i have being trya figure it 
        out for a while now. But i have ran out of time and dont want to get it in 
        late. If you find the problem while marking please could u let me know 
                
        */        
        
        
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        buttonFour = new javax.swing.JButton();
        buttonOne = new javax.swing.JButton();
        buttonThree = new javax.swing.JButton();
        buttonTwo = new javax.swing.JButton();
        enterBuutton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        textBox = new javax.swing.JTextArea();
        purchaseListingBox = new javax.swing.JTextField();
        labelBox = new javax.swing.JTextField();

        jLabel2.setText("jLabel2");

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        buttonFour.setText("4");
        buttonFour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFourActionPerformed(evt);
            }
        });

        buttonOne.setText("1");
        buttonOne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonOneActionPerformed(evt);
            }
        });

        buttonThree.setText("3");
        buttonThree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonThreeActionPerformed(evt);
            }
        });

        buttonTwo.setText("2");

        enterBuutton.setText("Enter");
        enterBuutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enterBuuttonActionPerformed(evt);
            }
        });

        textBox.setColumns(20);
        textBox.setRows(5);
        jScrollPane1.setViewportView(textBox);

        purchaseListingBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purchaseListingBoxActionPerformed(evt);
            }
        });

        labelBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                labelBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(56, Short.MAX_VALUE)
                .addComponent(enterBuutton, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonThree, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonOne, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(purchaseListingBox))
                    .addComponent(labelBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonFour, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonTwo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelBox, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonOne, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(buttonThree, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonTwo, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(buttonFour, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(purchaseListingBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(enterBuutton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonThreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonThreeActionPerformed
        //method for listing the shop class objects
        userOption = 3;
        
        //TODO find out how to add text to text box instead of set so i can loop this ....
        textBox.setText("Code\t Name\t Price\t Quantiy\t\n "
                + "0" + "\t " + shop.get(0).product + "\t " + shop.get(0).price + "\t"
        + shop.get(0).quantity + "\n"
                + "1" + "\t " + shop.get(1).product + "\t " + shop.get(1).price + "\t"
        + shop.get(1).quantity + "\n"
                + "2" + "\t " + shop.get(2).product + "\t " + shop.get(2).price + "\t"
        + shop.get(2).quantity + "\n"
                + "3" + "\t " + shop.get(3).product + "\t " + shop.get(3).price + "\t"
        + shop.get(3).quantity + "\n"
                + "4" + "\t " + shop.get(4).product + "\t " + shop.get(4).price + "\t"
        + shop.get(4).quantity + "\n"
                + "5" + "\t " + shop.get(5).product + "\t " + shop.get(5).price + "\t"
        + shop.get(5).quantity + "\n"
                + "6" + "\t " + shop.get(6).product + "\t " + shop.get(6).price + "\t"
        + shop.get(6).quantity + "\n"
                + "7" + "\t " + shop.get(7).product + "\t " + shop.get(7).price + "\t"
        + shop.get(7).quantity + "\n"
                + "8" + "\t " + shop.get(8).product + "\t " + shop.get(8).price + "\t"
        + shop.get(8).quantity + "\n");
    }//GEN-LAST:event_buttonThreeActionPerformed

    private void labelBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_labelBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_labelBoxActionPerformed

    private void purchaseListingBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purchaseListingBoxActionPerformed
        // list all of the items that are inside the basket ...
        // TODO figure out why this downt display ...

        
    }//GEN-LAST:event_purchaseListingBoxActionPerformed

    private void buttonOneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonOneActionPerformed
        //button for add item to the basket
        
        userOption = 1;
        labelBox.setText("Please enter the code of the item that you wish to add to your basket");
        textBox.setText("");
        
        
    }//GEN-LAST:event_buttonOneActionPerformed

    private void enterBuuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enterBuuttonActionPerformed
        //moethdo will user userOption vartiable to switch between enter options
        
        
        
        switch(userOption){
            case 1:
                // adding an item to the basket arraylist ...
                //TODO ??? dont know i look into weather the items are not being added to the basket array if that is the cause of the error ...
                itemCode = Integer.parseInt(textBox.getText());
                if(shop.get(itemCode).quantity >= 1){
                    basket.add(new Basket(itemNumber, shop.get(itemCode).product, 
                            shop.get(itemCode).price));
                            itemNumber++;
                            //removing 1 of the item that has being scanned ...
                            shop.get(itemCode).quantity--;
                            labelBox.setText("that item has being added to your basket");
                            
                            purchaseListingBox.setText(purchaseListingBox.getText() + Integer.toString(basket.get(basket.size()).code) +
                                     "\t " + basket.get(basket.size()).product + "\t " +
                                     Integer.toString(basket.get(basket.size()).price));
            }else{
                    labelBox.setText("I am sorry no more of that item in stock");
                }
                userOption = 0;
                textBox.setText("");
                break;
            case 2:
                
                
                userOption = 0;
                textBox.setText("");
                break;
            case 3:
                
                
                userOption = 0;
                textBox.setText("");
                break;
            case 4:
                double amountpayed = Double.parseDouble(textBox.getText());
                
                if(amountpayed >= totalCost){
                    labelBox.setText("Thanks for your purchase your change is " + 
                            (totalCost - amountpayed) + "your recipt has being made ...");
                    //used to reset the basket so that another shoper can use the program ( reset )
                    basket.clear();
                    userOption = 0;
                    textBox.setText("Please select one of the options below to begin \n"
                        + "1 - add an item to the basket \n"
                        + "2 - \n"
                        + "3 - look at the list of items in the store \n"
                        + "4 - checkout");
                    
                    
                        try {
                        //This area is for producing a list of the items that have being purchased ...

                            writer.write("Code\t Name\t Price \n\n");
                            for(int i = 0; i > basket.size(); i++){
                             writer.write("\n" + Integer.toString(basket.get(i).code) + "\t" + basket.get(i).product + 
                                     "\t" + Integer.toString(basket.get(i).price));   
                            }
                            writer.write("\n TotalCost = " + totalCost);
    
                        //NOTE dont really know what this does but it seems to fix the error ... 
                        } catch (IOException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        } 
                            
                }else{
                    labelBox.setText("I am sorry that is not enough money please enter \n"
                            + " a differnet amount");
                    textBox.setText("");
                }
                break;
 
        }
        
        
        
    }//GEN-LAST:event_enterBuuttonActionPerformed

    private void buttonFourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFourActionPerformed
        //method for finsihing the purchase
        
        userOption = 4;
        
        labelBox.setText("");
        
        //adding up all the costs of all the items in the basket class ... 
        for(int i = 0; i > basket.size(); i++){
            totalCost = totalCost + basket.get(i).price;
            assert (totalCost >= basket.size()): "Testing to make sure that prices are being added up properly";
        }
        
        int r = (int)(Math.random() * 10);
        
        //this is to test to make sure that the array is working ... ( not the problem )
        purchaseListingBox.setText("your total cost before disocunt is " + totalCost);
        
        //this is for working out the discount as asked for in the project specifications ...
        switch(r){
            case 1:
                totalCost = totalCost / 100;
                totalCost = totalCost * 90;
                assert ((5/100)*90 == 4.5): "Testing the disount section";
                break;
            case 2:
                totalCost = totalCost / 100;
                totalCost = totalCost * 80;
                break;
            case 3:
                totalCost = totalCost / 100;
                totalCost = totalCost * 70;
                break;
            case 4:
                totalCost = totalCost / 100;
                totalCost = totalCost * 60;
                break;
            case 5:
                totalCost = totalCost / 100;
                totalCost = totalCost * 50;
                break;
            case 6, 7, 8, 9, 10:
                if(totalCost >= 5 ){
                    totalCost = totalCost - 5;
                }
        }
        
        labelBox.setText("The total cost of your purchase\n "
                + "is after discount " + totalCost + "... please enter the amount you are paying");
        
        
    }//GEN-LAST:event_buttonFourActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
       

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonFour;
    private javax.swing.JButton buttonOne;
    private javax.swing.JButton buttonThree;
    private javax.swing.JButton buttonTwo;
    private javax.swing.JButton enterBuutton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField labelBox;
    private javax.swing.JTextField purchaseListingBox;
    private javax.swing.JTextArea textBox;
    // End of variables declaration//GEN-END:variables


}

class Shop{

    int code;
    String product;
    int price;
    int quantity;
    
    Shop(int c, String n, int p, int q){
        code = c;
        product = n;
        price = p;
        quantity = q;
    }  
 
    int getQuantity(){
        return quantity;
    }
   
    
}

    
class Basket{

    int code;
    String product;
    int price;
    int quantity;
    
    Basket(int c, String n, int p){
        code = c;
        product = n;
        price = p;
    }  
    
    void addItem(){
        
        
        
    }
    
    
}